XNATX SchemaDocs Module
=======================

The XNATX SchemaDocs module provides an integrated REST-based service to
generate documentation from the XNAT data-type schema that are installed in your
deployed XNAT application.

Installation
------------

SchemaDocs is a standard XNAT module that can be installed by placing the
generated module archive in the folder specified as the XNAT modules folder in
your **build.properties** file:

1.  If building SchemaDocs [from
    source](<https://bitbucket.org/xnatx/schemadocs>), you can just type **mvn
    clean package** in the SchemaDocs source folder. This will generate a file
    named **target/xnatx-schemadocs-***version***.jar**. Otherwise, download the
    module archive from the XNAT Marketplace or other source.

2.  Copy the module archive into the folder specified by the
    **xdat.modules.location** property in your XNAT **build.properties** file.

3.  Run XNAT’s **update.sh** or **update.bat** script as described in the [XNAT
    upgrade
    documentation](<https://wiki.xnat.org/display/XNAT16/How+to+Upgrade+XNAT>),
    including updating the database from the generated SQL update script.

4.  Re-start XNAT.

Usage
-----

Once you’ve installed the SchemaDocs module, viewing a schema’s documentation is
accomplished through a REST call.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
http://server/data/services/schemadoc
http://server/data/services/schemadoc/{SCHEMA}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The first call is the same as the second, but just uses the default value of
**xnat** for the schema name, so is the equivalent of

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
http://server/data/services/schemadoc/xnat
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The value for *SCHEMA* should be the name of the XSD schema file you want to
render. This function assumes that the schema is located within the deployed
XNAT web application in the folder:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
schemas/SCHEMA/SCHEMA.xsd
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There is not currently support for schema definitions located in folders named
differently from the data-type schema definition.

Acknowledgments
---------------

Special thanks to Matthew South of Oxford University for creating and sharing
the **schema-hierarchy.xslt** transform document at the heart of the SchemaDocs
module.

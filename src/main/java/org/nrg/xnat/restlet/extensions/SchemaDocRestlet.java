package org.nrg.xnat.restlet.extensions;

import org.apache.commons.lang.StringUtils;
import org.nrg.xft.XFT;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.resource.Representation;
import org.restlet.resource.StringRepresentation;
import org.restlet.resource.Variant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.nio.file.Paths;

@XnatRestlet({"/services/schemadoc", "/services/schemadoc/{SCHEMA}"})
public class SchemaDocRestlet extends SecureResource {

    public SchemaDocRestlet(Context context, Request request, Response response) {
        super(context, request, response);
        getVariants().add(new Variant(MediaType.TEXT_HTML));
        _schema = (String) getRequest().getAttributes().get(PARAM_SCHEMA);
        _log.info("Received request to render a schema: " + getSchemaPath());
    }

    @Override
    public Representation represent(final Variant variant) {
        final MediaType mediaType = overrideVariant(variant);
        return new StringRepresentation(transform(), mediaType);
    }

    private String transform() {
        final File xslt = new File(XFT.GetConfDir(),  "schema-hierarchy.xslt");
        final File xsd = Paths.get(XFT.GetConfDir(), "../..", getSchemaPath()).toFile();
        try (final FileReader reader = new FileReader(xslt);
             final BufferedInputStream schema = new BufferedInputStream(new FileInputStream(xsd))) {

            Transformer transformer = TransformerFactory.newInstance().newTransformer(new StreamSource(reader));

            final StringWriter writer = new StringWriter();

            transformer.transform(new StreamSource(schema), new StreamResult(writer));

            return writer.toString();
        } catch (TransformerException e) {
            return e.getMessage() + "\n" + e.getLocationAsString();
        } catch (FileNotFoundException e) {
            return "Couldn't find the specified file: " + e.getMessage();
        } catch (IOException e) {
            return "An error occurred trying to transform the schema: " + e.getMessage();
        }
    }

    private String getSchemaPath() {
        return StringUtils.isNotBlank(_schema) ? String.format(SCHEMA_PATH, _schema, _schema) : "schemas/xnat/xnat.xsd";
    }

    private static final Logger _log = LoggerFactory.getLogger(SchemaDocRestlet.class);
    private static final String PARAM_SCHEMA = "SCHEMA";
    private static final String SCHEMA_PATH = "schemas/%s/%s.xsd";

    private final String _schema;
}
